import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import selenium.webdriver.support.ui as ui

class testAddFlyers(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("http://localhost:12080/addflyer")
        self.driver.find_element_by_name("admin").click()
        self.driver.find_element_by_id("submit-login").click()
        self.driver.find_element_by_link_text('Events & Flyers').click()

    def test_logged_in(self):
        driver = self.driver
        print driver.title
        bodyText = driver.find_element_by_tag_name('body').text
        #print bodyText
        self.assertTrue("A listing of personal events" in bodyText)

    def test_flyers_title(self):
        driver = self.driver
        driver.find_element_by_id("addFlyerLink").click()
        assert "Flyer Upload" in driver.title

    # Todo Update when some image upload work happens
    # def test_add_flyer(self):
    #    driver = self.driver
    #    driver.find_element_by_id("addFlyerLink").click()
    #    elem = driver.find_element_by_id("description")
    #    elem.click()
    #    elem.send_keys('Testing This works')
    #    elem = driver.find_element_by_id("upload").click()
    #    bodyText = driver.find_element_by_tag_name('body').text
    #    print bodyText
    #    self.assertTrue("The resource could not be found." in bodyText)

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()
