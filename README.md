This folder contains some sample tests, and will eventually contain a test runner
as well as a results export.

Requirements:

- Sauce Labs account and environment variables on local machine
- Selenium Server
- Python 2.7
