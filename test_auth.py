import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class testAuth(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("http://localhost:12080")

    def test_auth_info(self):
        driver = self.driver
        assert "Login" in driver.title

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()
