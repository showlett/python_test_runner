import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class testFlyers(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("http://localhost:12080")

    def test_flyers_title(self):
        driver = self.driver
        assert "Login" in driver.title
        elem = driver.find_element_by_name("admin")
        elem.click()
        driver.find_element_by_id("submit-login").click()
        print driver.title
        driver.find_element_by_link_text("Events & Flyers").click()
        assert "Flyers" in driver.title

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()
