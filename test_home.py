import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class test_home(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("http://localhost:12080")

    def test_home_login(self):
        driver = self.driver
        assert "Login" in driver.title
        print "First Check " + driver.title
        elem = driver.find_element_by_name("admin")
        elem.click()
        driver.find_element_by_id("submit-login").click()
        print "Second Check " + driver.title
        assert "S-Howlett Dev Scripts.." in driver.title


   # def tearDown(self):
    #    self.driver.close()

if __name__ == "__main__":
    unittest.main()

