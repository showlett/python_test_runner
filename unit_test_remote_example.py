# this is a basic example that with the selenium server running,
# will run a test using the remote
# this will give you selenium logging etc in the terminal.

# first import is a pycharm fix
from __future__ import absolute_import
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Remote(
            command_executor='http://127.0.0.1:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.CHROME)

    def test_search_in_python_org(self):
        driver = self.driver
        driver.get("http:www.python.org")
        self.assertIn("Python", driver.title)
        print "Window title is " + driver.title
        elem = driver.find_element_by_name("q")
        #uses clear to ensure that the search box is clean, not appended to
        elem.clear()
        elem.send_keys('pycon')
        elem.send_keys(Keys.RETURN)
        assert "No Results found" not in driver.page_source

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()
