# this is a basic example that will run tests on saucelabs
# adds name, tags, build etc.

# first import is a pycharm fix
from __future__ import absolute_import
import os
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from sauceclient import SauceClient

# sauce access requirements
# it's best to remove the hardcoded defaults and always get these values
# from environment variables

USERNAME = os.environ.get('SAUCE_USERNAME')
ACCESS_KEY = os.environ.get('SAUCE_ACCESS_KEY')
sauce = SauceClient(USERNAME, ACCESS_KEY)


class SimpleTestCase(unittest.TestCase):

    def setUp(self):
        desired_cap = {
            'name': "Sauce Health Check",
            'platform': "Windows 7",
            'browserName': "Internet Explorer",
            'version': "11.0",
            'build': "build-1234",
            'tags': ["Testing Sauce Tunnel, Please ignore"]}

        sauce_url = "http://%s:%s@ondemand.saucelabs.com:80/wd/hub"
        self.driver = webdriver.Remote(
            command_executor=sauce_url % (USERNAME, ACCESS_KEY),
            desired_capabilities=desired_cap)

    def test_get_title(self):
        driver = self.driver
        driver.get("http://localhost:12080/")
        self.assertIn("Login", driver.title)
        elem = driver.find_element_by_name("admin")
        elem.click()
        driver.find_element_by_id("submit-login").click()
        self.assertIn("S-Howlett Dev Scripts..", driver.title)
        print("Link to your job: https://saucelabs.com/jobs/%s" % driver.session_id)
        sauce.jobs.update_job(driver.session_id, passed=True)

    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
