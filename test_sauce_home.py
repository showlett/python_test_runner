import os
import sys
import new
import unittest
from selenium import webdriver
from sauceclient import SauceClient

# it's best to remove the hardcoded defaults and always get these values
# from environment variables
USERNAME = os.environ.get('SAUCE_USERNAME')
ACCESS_KEY = os.environ.get('SAUCE_ACCESS_KEY')
sauce = SauceClient(USERNAME, ACCESS_KEY)

# Edge Case is currently failing

browsers = [{"platform": "Mac OS X 10.9",
             "browserName": "chrome",
             "version": "55"},
            {"platform": "Windows 10",
             "browserName": "MicrosoftEdge",
             "version": "14.14393"},
            {"platform": "Windows 7",
             "browserName": "internet explorer",
             "version": "11.0"},
            {"platform": "Windows 8.1",
             "browserName": "internet explorer",
             "version": "11.0"}]

def on_platforms(platforms):
    def decorator(base_class):
        module = sys.modules[base_class.__module__].__dict__
        for i, platform in enumerate(platforms):
            d = dict(base_class.__dict__)
            d['desired_capabilities'] = platform
            name = "%s_%s" % (base_class.__name__, i + 1)
            module[name] = new.classobj(name, (base_class,), d)
    return decorator

@on_platforms(browsers)
class SauceConcurrencyTest(unittest.TestCase):
    def setUp(self):
        self.desired_capabilities['name'] = "Simon Concurrency Test"
        # = self.id() (this adds the full test path and number to the name value)
        self.desired_capabilities['tags'] = ["Testing Sauce Tunnel Please ignore"]
        sauce_url = "http://%s:%s@ondemand.saucelabs.com:80/wd/hub"
        self.driver = webdriver.Remote(
            desired_capabilities=self.desired_capabilities,
            command_executor=sauce_url % (USERNAME, ACCESS_KEY)
        )
        self.driver.get('http://localhost:12080')

    def test_steps(self):
        driver = self.driver
        assert "Login" in driver.title
        elem = driver.find_element_by_name("admin")
        elem.click()
        driver.find_element_by_id("submit-login").click()
        assert "S-Howlett Dev Scripts.." in driver.title

    def tearDown(self):
        print("Link to your job: https://saucelabs.com/jobs/%s" % self.driver.session_id)
        try:
            if sys.exc_info() == (None, None, None):
                sauce.jobs.update_job(self.driver.session_id, passed=True)
            else:
                sauce.jobs.update_job(self.driver.session_id, passed=False)
        finally:
            self.driver.quit()
